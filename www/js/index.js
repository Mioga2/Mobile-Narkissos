/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    create: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
		switch (id) {
			case 'deviceready':
				$('div.splash').fadeOut ('slow', function () {
					app.initialize ();
				});
				break;
		}
    },
	initialize: function () {
		$.ajax ({
			type: 'GET',
			url: 'config.json',
			dataType: 'JSON',
			error: function (data) {
				alert ('Error getting configuration');
			},
			success: function (data) {
				app.mioga = new Mioga (data);

				// Initialize login system
				var login = new MiogaLogin ({$el: $('div.app'), mioga: app.mioga});

				// Switch to application logic
				app.run ();
			}
		});
	},
	run: function () {
		// TODO place application logic here
		app.mioga.router.route ('main', function () {
			$('.app').empty ();
		});
	}
};

app.create ();
